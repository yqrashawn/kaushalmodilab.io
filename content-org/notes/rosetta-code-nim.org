#+setupfile: ../scripter-setupfile.org

#+title: Rosetta Code
#+date: 2018-06-04

#+hugo_auto_set_lastmod: t

#+hugo_base_dir: ../../
#+hugo_section: notes
#+hugo_bundle: rosetta-code
#+export_file_name: index

#+hugo_categories: programming
#+hugo_tags: nim examples rosetta-code

#+hugo_custom_front_matter: :versions '((nim . "0.18.1, git hash: 91765e583d"))

* Bernoulli Numbers
[[https://www.rosettacode.org/wiki/Bernoulli_numbers][Page]]
#+begin_src nim
import rationals
echo 0.33.toRational
#+end_src

#+RESULTS:
: 33/100

#+begin_src nim
# Need to first do "nimble install bignum"
import strformat, bignum
const upper = 61
var bn: array[upper, Rat]

proc bernoulli(n: int): Rat =
  var A: seq[Rat]

  for i in 0 .. n:
    A.add(newRat())

  for i in 0 .. A.high:
    discard A[i].set(1, i+1)
    for j in countDown(i, 1):
      A[j-1] = j*(A[j-1] - A[j])

  return A[0] # (which is Bn)

for i in 0 .. bn.high:
  bn[i] = bernoulli(i)
  if bn[i].toFloat != 0.0:
    echo fmt"B({i:2}) = {bn[i]:>55}"
#+end_src

#+RESULTS:
#+begin_example
B( 0) =                                                       1
B( 1) =                                                     1/2
B( 2) =                                                     1/6
B( 4) =                                                   -1/30
B( 6) =                                                    1/42
B( 8) =                                                   -1/30
B(10) =                                                    5/66
B(12) =                                               -691/2730
B(14) =                                                     7/6
B(16) =                                               -3617/510
B(18) =                                               43867/798
B(20) =                                             -174611/330
B(22) =                                              854513/138
B(24) =                                         -236364091/2730
B(26) =                                               8553103/6
B(28) =                                        -23749461029/870
B(30) =                                     8615841276005/14322
B(32) =                                      -7709321041217/510
B(34) =                                         2577687858367/6
B(36) =                           -26315271553053477373/1919190
B(38) =                                      2929993913841559/6
B(40) =                            -261082718496449122051/13530
B(42) =                             1520097643918070802691/1806
B(44) =                            -27833269579301024235023/690
B(46) =                            596451111593912163277961/282
B(48) =                     -5609403368997817686249127547/46410
B(50) =                          495057205241079648212477525/66
B(52) =                    -801165718135489957347924991853/1590
B(54) =                    29149963634884862421418123812691/798
B(56) =                 -2479392929313226753685415739663229/870
B(58) =                 84483613348880041862046775994036021/354
B(60) =   -1215233140483755572040304994079820246041491/56786730
#+end_example
** Solution by Lando on Nim Forum
[[https://forum.nim-lang.org/t/3889#24208][ref]]

{{{nuser(Lando)}}} provided a solution based of my solution using the
[[https://github.com/FedeOmoto/bignum][bignum]] library.
#+begin_src nim
# Need to first do "nimble install bignum"
import strformat, bignum

const upper = 61
var bn: array[upper, Rat]

proc bernoulli(n: int): Rat =
  var A: seq[Rat] = @[]

  for i in 0 .. n:
    A.add(newRat())

  for i in 0 .. A.high:
    discard A[i].set(1, i+1)
    for j in countDown(i, 1):
      A[j-1] = j*(A[j-1] - A[j])

  return A[0] # (which is Bn)

for i in 0 .. bn.high:
  bn[i] = bernoulli(i)
  if bn[i].toFloat != 0.0:
    echo fmt"B({i:2}) = {bn[i]:>50}"
#+end_src

#+RESULTS:
#+begin_example
B( 0) =                                                  1
B( 1) =                                                1/2
B( 2) =                                                1/6
B( 4) =                                              -1/30
B( 6) =                                               1/42
B( 8) =                                              -1/30
B(10) =                                               5/66
B(12) =                                          -691/2730
B(14) =                                                7/6
B(16) =                                          -3617/510
B(18) =                                          43867/798
B(20) =                                        -174611/330
B(22) =                                         854513/138
B(24) =                                    -236364091/2730
B(26) =                                          8553103/6
B(28) =                                   -23749461029/870
B(30) =                                8615841276005/14322
B(32) =                                 -7709321041217/510
B(34) =                                    2577687858367/6
B(36) =                      -26315271553053477373/1919190
B(38) =                                 2929993913841559/6
B(40) =                       -261082718496449122051/13530
B(42) =                        1520097643918070802691/1806
B(44) =                       -27833269579301024235023/690
B(46) =                       596451111593912163277961/282
B(48) =                -5609403368997817686249127547/46410
B(50) =                     495057205241079648212477525/66
B(52) =               -801165718135489957347924991853/1590
B(54) =               29149963634884862421418123812691/798
B(56) =            -2479392929313226753685415739663229/870
B(58) =            84483613348880041862046775994036021/354
B(60) = -1215233140483755572040304994079820246041491/56786730
#+end_example
** Python
[[https://www.rosettacode.org/wiki/Bernoulli_numbers#Python][Source]]
#+begin_src python
from fractions import Fraction as Fr

def bernoulli(n):
    A = [0] * (n+1)
    for m in range(n+1):
        A[m] = Fr(1, m+1)
        for j in range(m, 0, -1):
          A[j-1] = j*(A[j-1] - A[j])
    return A[0] # (which is Bn)

bn = [(i, bernoulli(i)) for i in range(61)]
bn = [(i, b) for i,b in bn if b]
width = max(len(str(b.numerator)) for i,b in bn)
for i,b in bn:
    print('B(%2i) = %*i/%i' % (i, width, b.numerator, b.denominator))
#+end_src

#+RESULTS:
#+begin_example
B( 0) =                                            1/1
B( 1) =                                            1/2
B( 2) =                                            1/6
B( 4) =                                           -1/30
B( 6) =                                            1/42
B( 8) =                                           -1/30
B(10) =                                            5/66
B(12) =                                         -691/2730
B(14) =                                            7/6
B(16) =                                        -3617/510
B(18) =                                        43867/798
B(20) =                                      -174611/330
B(22) =                                       854513/138
B(24) =                                   -236364091/2730
B(26) =                                      8553103/6
B(28) =                                 -23749461029/870
B(30) =                                8615841276005/14322
B(32) =                               -7709321041217/510
B(34) =                                2577687858367/6
B(36) =                        -26315271553053477373/1919190
B(38) =                             2929993913841559/6
B(40) =                       -261082718496449122051/13530
B(42) =                       1520097643918070802691/1806
B(44) =                     -27833269579301024235023/690
B(46) =                     596451111593912163277961/282
B(48) =                -5609403368997817686249127547/46410
B(50) =                  495057205241079648212477525/66
B(52) =              -801165718135489957347924991853/1590
B(54) =             29149963634884862421418123812691/798
B(56) =          -2479392929313226753685415739663229/870
B(58) =          84483613348880041862046775994036021/354
B(60) = -1215233140483755572040304994079820246041491/56786730
#+end_example
