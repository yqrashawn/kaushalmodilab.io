+++
title = "ઈમૅક્સ મા ગુજરાતી – Gujarati in Emacs"
author = ["Kaushal Modi"]
tags = ["gujarati"]
categories = ["emacs"]
draft = true
creator = "Emacs 27.0.50 (Org mode 9.2.3 + ox-hugo)"
+++

-   Map of India with Gujarat highlighted
    -   <https://en.wikipedia.org/wiki/Gujarat>
    -   {{< figure src="https://en.wikipedia.org/wiki/File:IN-GJ.svg" >}}
-   Gujarati language - <https://en.wikipedia.org/wiki/Gujarati%5Flanguage>
-   Gujarati script - <https://en.wikipedia.org/wiki/Gujarati%5Falphabet>
    -   The Unicode block for Gujarati is U+0A80 -- U+0AFF -- [Ref](https://www.unicode.org/charts/PDF/U0A80.pdf)

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
