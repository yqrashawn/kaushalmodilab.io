+++
title = "Org Table Spreadsheet"
author = ["Kaushal Modi"]
description = "My notes on Org Table Spreadsheet feature."
date = 2018-04-02
lastmod = 2018-11-05T11:27:46-05:00
tags = ["table", "spreadsheet"]
categories = ["emacs", "org"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.1.14 + ox-hugo)"
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Normalizing](#normalizing)
    - [Equations Breakdown](#equations-breakdown)
    - [Evaluate the whole table](#evaluate-the-whole-table)
- [References](#references)

</div>
<!--endtoc-->



## Normalizing {#normalizing}

<div class="ox-hugo-table wide-table">
<div></div>

<a id="table--org-table-norm"></a>
<div class="table-caption">
  <span class="table-number"><a href="#table--org-table-norm">Table 1</a></span>:
  Org table Normalizing
</div>

|          | Normalized col `$1` |             | Normalized col `$3` | Ratio of col `$2` / `$4` |
|----------|---------------------|-------------|---------------------|--------------------------|
| Col `$1` | Col `$2`            | Col `$3`    | Col `$4`            | Col `$5`                 |
| 79922.1  | 6.85                | 0.146993824 | 4.27                | 1.60                     |
| 79185.9  | 6.78                | 0.146976008 | 4.27                | 1.59                     |
| 64041.0  | 5.48                | 0.147067031 | 4.27                | 1.28                     |
| 15452.5  | 1.32                | 0.034456713 | 1.00                | 1.32                     |
| 11675.7  | 1.00                | 0.034460162 | 1.00                | 1.00                     |

</div>


### Equations Breakdown {#equations-breakdown}

Equation: `#+tblfm: $2=$1/@7$1;%0.2f::$4=$3/@7$3;%0.2f::$5=$2/$4;%0.2f`

-   `$2=$1/@7$1` -- Assign the value of a field in **column** 2 (`$2`) to
    the value of the field in **column** 1 **in the same row** (`$1`)
    divided by the value of the field in **row** 7, **column** 1 (`@7$1`).
    -   `@` is row, `$` is column.
    -   Row and column numbers start with **1** (not 0).
-   `$4=$3/@7$3` -- Similar to above, but with different row/column numbers.
-   `$5=$2/$4` -- Assign the value of a field in **column** 5 (`$5`) to the
    value of the field in **column** 2 **in the same row** (`$2`) divided by
    the value of the field in **column** 4 **in the same row** as well (`$4`).
-   `;` is used to separate the equation from the _formatting_.
-   For all the 3 equations, formatting is applied using `%0.2f`
    i.e. the numbers will be formatted with 2 decimal places. See [Field
    Formatters in Org table](https://scripter.co/field-formatters-in-org-table/) for more info.
-   Each of those equations are applied to the same table by
    concatenating them with `::`.


### Evaluate the whole table {#evaluate-the-whole-table}

Do `C-u C-c C-c` with point anywhere in the table, or do `C-c C-c`
with point on the `#+tblfm` line.


## References {#references}

-   <a href="https://www.gnu.org/software/emacs/manual/html_node/org/Formula-syntax-for-Calc.html"><abbr title="Read the same section within Emacs by doing 'C-h i g (org) Formula syntax for Calc'">(org) Formula syntax for Calc</abbr></a>

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
