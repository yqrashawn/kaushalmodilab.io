+++
title = "SystemVerilog"
author = ["Kaushal Modi"]
description = "Arbitrary SystemVerilog snippets"
date = 2018-11-05T00:00:00-05:00
lastmod = 2019-07-17T12:05:44-04:00
tags = ["systemverilog"]
categories = ["programming"]
draft = false
creator = "Emacs 27.0.50 (Org mode 9.2.3 + ox-hugo)"
[versions]
  systemverilog = "IEEE 1800-2017"
  xcelium = "18.03-s006"
+++

<div class="ox-hugo-toc toc">
<div></div>

<div class="heading">Table of Contents</div>

- [Queues and Arrays](#queues-and-arrays)
    - [Associative Arrays](#associative-arrays)
- [Introspection](#introspection)
    - [Printing type/class of a variable/object](#typename)
- [Types](#types)
    - [Enums](#enums)
- [Template](#template)

</div>
<!--endtoc-->



## Queues and Arrays {#queues-and-arrays}


### Associative Arrays {#associative-arrays}

Section 7.8 **Associative arrays** from SystemVerilog standard IEEE 1800-2017

<a id="code-snippet--assoc-arrays-index-val-queue"></a>
```systemverilog
typedef enum {SOURCE_REF, SOURCE_AUX, SOURCE_NCO, SOURCE_UTS, SOURCE_FBK} kind_t;

//  +--------------------+--> For assoc array element «KEY: VAL», VAL is a queue ($) of type "int"
//  |                    |
// int kind_map [kind_t][$] =
//     ^         ^
//     |         |
//     |         +-- For assoc array element «KEY: VAL», KEY is of type "kind_t"
//     |
//     +-- Variable name of the assoc array

int kind_map [kind_t][$] = '{
                             SOURCE_REF: '{1, 2, 3},
                             SOURCE_AUX: '{2, 3, 4},
                             SOURCE_NCO: '{3, 4, 5},
                             SOURCE_UTS: '{4, 5, 6},
                             SOURCE_FBK: '{5, 6, 7}
                             };

initial begin
  $display("%0d entries", kind_map.num);
  $display("kind_map = %p", kind_map);
  foreach (kind_map[i]) begin
    $display("kind_map[%p] = %p", i, kind_map[i]);
    $display("  key = %p (type = %s)", i, $typename(i));
    $display("  value = %p (type = %s)", kind_map[i], $typename(kind_map[i]));
  end
end
```

<div class="src-block-caption">
  <span class="src-block-number"><a href="#code-snippet--assoc-arrays-index-val-queue">Code Snippet 1</a></span>:
  Associative arrays where index values are queues
</div>


## Introspection {#introspection}


### Printing type/class of a variable/object {#typename}

Section 20.6.1 **Type name function** from SystemVerilog standard IEEE 1800-2017

See the use of `$typename` in [Code Snippet 1](#code-snippet--assoc-arrays-index-val-queue).


## Types {#types}


### Enums {#enums}


#### Number of enum elements {#number-of-enum-elements}

Section 6.19.5.5 **Num()** from SystemVerilog standard IEEE 1800-2017

```systemverilog
typedef enum {SOURCE_REF, SOURCE_AUX, SOURCE_NCO, SOURCE_UTS, SOURCE_FBK} kind_t;

kind_t foo;

initial begin
  $display("number of elements in kind_t: %0d", foo.num());
end
```


#### Loop through all enum values {#loop-through-all-enum-values}

```systemverilog
typedef enum {SOURCE_REF, SOURCE_AUX, SOURCE_NCO, SOURCE_UTS, SOURCE_FBK} kind_t;

kind_t foo;

initial begin
  foo = foo.first();
  while (1) begin
    $display("foo = %s", foo.name());
    if (foo == foo.last()) break;
    else foo = foo.next();
  end
end
```


## Template {#template}

```systemverilog
`timescale 1ns/1ps

module top;

  // <custom code here>

  // Sim duration
  initial begin
    #400ns;
    $finish;
  end

  // timeformat and wave config
  initial begin
    $timeformat(-9, 3, "ns");  // units, precision, suffix
    $shm_open("waves.shm");
    $shm_probe("ACTM");
  end

endmodule: top
```

[//]: # "Exported with love from a post written in Org mode"
[//]: # "- https://github.com/kaushalmodi/ox-hugo"
